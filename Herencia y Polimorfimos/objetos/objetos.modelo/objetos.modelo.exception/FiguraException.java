package objetos.modelo.exception;

public class FiguraException extends Exception {

	private static final long serialVersionUID = 1L;

	public FiguraException(String message) {
		super(message);
	}
}
