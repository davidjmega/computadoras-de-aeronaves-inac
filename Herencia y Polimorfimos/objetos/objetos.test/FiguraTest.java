package objetos.test;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import objetos.modelo.Circulo;
import objetos.modelo.Cuadrado;
import objetos.modelo.Figura;
import objetos.modelo.Poligono;
import objetos.modelo.Rectangulo;
import objetos.modelo.Triangulo;

public class FiguraTest {
	List<Figura> lstFiguras = new ArrayList<Figura>();
	Set<Figura>  setFiguras = new HashSet<Figura>();

	Cuadrado 	cuadrado 	;
	Circulo 	circulo		;
	Rectangulo  rectangulo	;
	Triangulo   triangulo   ;
	Poligono    poligono    ;
	
	@Before
	public void setUp() throws Exception {
		cuadrado = new Cuadrado("cuadradoTest", 10);
		circulo = new Circulo("CirculoTest", 10);
		rectangulo = new Rectangulo("rectanguloTest", 10, 15);
		triangulo = new Triangulo("trianguloTest", 5, 15);
		poligono = new Poligono("PoligonoTest", 6, 8, 12);

		lstFiguras.add(cuadrado);
		lstFiguras.add(circulo);
		lstFiguras.add(rectangulo);
		lstFiguras.add(triangulo);
		lstFiguras.add(poligono);
		lstFiguras.add(new Cuadrado("cuadrado2", 20));
		lstFiguras.add(new Circulo("circulo2", 20));
		lstFiguras.add(new Rectangulo("rectangulo2", 20, 10));
		lstFiguras.add(new Triangulo("triangulo2", 20, 10));
		lstFiguras.add(new Poligono("poligono2", 20, 10, 5));

		setFiguras.add(cuadrado);
		setFiguras.add(circulo);
		setFiguras.add(rectangulo);
		setFiguras.add(triangulo);
		setFiguras.add(poligono);
		setFiguras.add(new Cuadrado("cuadrado2", 20));
		setFiguras.add(new Circulo("circulo2", 20));
		setFiguras.add(new Rectangulo("rectangulo2", 20, 10));
		setFiguras.add(new Triangulo("triangulo2", 20, 10));
		setFiguras.add(new Poligono("poligono2", 20, 10, 5));
	}

	@After
	public void tearDown() throws Exception {
		cuadrado = null;
		circulo = null;
		rectangulo = null;
		triangulo = null;
		poligono = null;
		
		lstFiguras = null;
		setFiguras = null;
	}
	@Test
	public void testMaximaSuperficie(){
		Assert.assertEquals(1256.64, Figura.getMaximaSuperficie(),0.01);
		
	}
	@Test 
	public void testContructorNobreRectangulo(){
		Assert.assertEquals("rectanguloTest", rectangulo.getNombre());
	}
	
	@Test 
	public void testContructorBaseRectangulo(){
		Assert.assertEquals(10.0f, rectangulo.getBase(),0.01);
	}
	
	@Test 
	public void testContructorAlturaRectangulo(){
		Assert.assertEquals(15.0f, rectangulo.getAltura(),0.01);
	}
	@Test
	public void testCalcularPerimetroRectangulo(){
		Assert.assertEquals(50.0f, rectangulo.calcularPerimetro(),0.01);
	}

	@Test
	public void testCalcularSuperficieRectangulo(){
		Assert.assertEquals(150.0f, rectangulo.calcularSuperficie(),0.01);
	}
	
	@Test
	public void testGetNombreDelCuadradoDelConstructor() {
		Assert.assertEquals("cuadradoTest", cuadrado.getNombre());
		
	}
	@Test
	public void testGelLadoDelCuadrado(){
		Assert.assertEquals(10.0f, cuadrado.getLado(), 0.1);
	}

	@Test
	public void testCalcularPerimetroDelCuadrado() {
		Assert.assertEquals(40.0f, cuadrado.calcularPerimetro(),0.1);
	}

	@Test
	public void testCalcularSuperficieDelCuadrado() {
		Assert.assertEquals(100.0f, cuadrado.calcularSuperficie(), 0.1);
	}
	
	@Test
	public void testContieneLaListaUnCuadrado(){
		Assert.assertTrue(lstFiguras.contains(cuadrado));
	}
	
	@Test
	public void testGetNombreDelTrianguloDelConstructor() {
		Assert.assertEquals("trianguloTest", triangulo.getNombre());
		
	}
	
	@Test
	public void testGetBaseDelTriangulo(){
		Assert.assertEquals(5.0f, triangulo.getBase(), 0.1);
	}

	@Test
	public void testGetAlturaDelTriangulo(){
		Assert.assertEquals(15.0f, triangulo.getAltura(), 0.1);
	}
	
	@Test
	public void testCalcularSuperfieDelTriangulo() {
		Assert.assertEquals(37.5f, triangulo.calcularSuperficie(), 0.1);
	}
	
	
	@Test
	public void testCalcularPerimetroDelTriangulo(){
		Assert.assertEquals(35.811, triangulo.calcularPerimetro(), 0.001);
	}
	@Test
	public void testContieneLaListaUnTriangulo(){
		Assert.assertTrue(lstFiguras.contains(triangulo));
	}

	@Test
	public void testGetNombreDelCirculoDelConstructor() {
		Assert.assertEquals("CirculoTest", circulo.getNombre());
		
	}
	@Test
	public void testGetRadioDelCirculo(){
		Assert.assertEquals(10.0f, circulo.getRadio(), 0.1);
	}

	@Test
	public void testCalcularSuperfieDelCirculo() {
		Assert.assertEquals(314.159f, circulo.calcularSuperficie(), 0.1);
	}
	
	@Test
	public void testCalcularPerimetroDelCirculo(){
		Assert.assertEquals(62.832, circulo.calcularPerimetro(), 0.001);
	}
	
	@Test
	public void testContieneLaListaUnCirculo(){
		Assert.assertTrue(lstFiguras.contains(circulo));
	}
	
	@Test
	public void testGetNombreDelPoligonoDelConstructor() {
		Assert.assertEquals("PoligonoTest", poligono.getNombre());
		
	}
	
	@Test
	public void testGetApotemaDelPoligono(){
		Assert.assertEquals(6.0f, poligono.getApotema(), 0.1);
	}

	@Test
	public void testGetCantidadDeLadosDelPoligono(){
		Assert.assertEquals(8.0f, poligono.getCantidadDeLados(), 0.1);
	}
	
	@Test
	public void testGetLadoDelPoligono(){
		Assert.assertEquals(12.0f, poligono.getLado(), 0.1);
	}
	
	@Test
	public void testCalcularSuperfieDelPoligono() {
		Assert.assertEquals(288.0f, poligono.calcularSuperficie(), 0.1);
	}
	
	
	@Test
	public void testCalcularPerimetroDelPoligono(){
		Assert.assertEquals(96.0, poligono.calcularPerimetro(), 0.001);
	}
	@Test
	public void testContieneLaListaUnPoligono(){
		Assert.assertTrue(lstFiguras.contains(poligono));
	}

	@Test
	public void testNOContieneLaListaUnCuadrado(){
		
		Assert.assertFalse(lstFiguras.contains(new Cuadrado("otro cuad",10)));
	}

	@Test
	public void testContieneLaListaUncirculo(){
		Assert.assertTrue(lstFiguras.contains(circulo));
	}
	
	@Test
	public void testNOContieneLaListaUnCirculo(){
		
		Assert.assertFalse(lstFiguras.contains(new Circulo("otro cir",10)));
	}
	
	@Test
	public void testContieneLaListaUnRectangulo(){
		Assert.assertTrue(lstFiguras.contains(rectangulo));
	}
	
	@Test
	public void testNOContieneLaListaUnRectangulo(){
		
		Assert.assertFalse(lstFiguras.contains(new Rectangulo("otro rec",20,10)));
	}
	
	
	@Test
	public void testNOContieneLaListaUnTriangulo(){
		Assert.assertTrue(lstFiguras.contains(triangulo));
	}
	
	@Test
	public void testNOContieneLaListaUnPoligono(){
		Assert.assertTrue(lstFiguras.contains(poligono));
	}
	
	@Test
	public void testEliminaCuadrado(){
		lstFiguras.remove(cuadrado);
		Assert.assertEquals(9,lstFiguras.size() );
	}
	@Test
	public void testNOContieneLaListaUncirculo(){
		Assert.assertFalse(lstFiguras.contains(new Circulo("otro Circulo",10)));
	}
	
	@Test
	public void testAgregarALista(){
		lstFiguras.add(new Cuadrado("cuadradoTest", 10));
		Assert.assertEquals(11, lstFiguras.size());
		
	}
	@Test
	public void testAgregaraSet(){
		setFiguras.add(new Cuadrado("cuadradoTest", 10));
		Assert.assertEquals(10, setFiguras.size());
		
	}
	
	@Test
	public void testGetValoresCuadrado(){
		Assert.assertEquals("l=10.0", cuadrado.getValores());
	}
	@Test
	public void testGetValoresCiculo(){
		Assert.assertEquals("r=10.0", circulo.getValores());
	}
}
