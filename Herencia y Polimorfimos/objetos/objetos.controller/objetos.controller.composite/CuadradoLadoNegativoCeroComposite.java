package objetos.controller.composite;
import objetos.modelo.Cuadrado;
public class CuadradoLadoNegativoCeroComposite extends ValidatorComposite {

	public CuadradoLadoNegativoCeroComposite() {
	}

	@Override
	public boolean isMe() {
		return figura instanceof Cuadrado;
	}

	@Override
	public boolean validar() {
		Cuadrado cua = (Cuadrado)figura;
		return cua.getLado()<=0;
	}

	@Override
	public String getError() {
		return "EL LADO QUE DEBE SER MAYO QUE 0";
	}
}