package objetos.controller.composite;
import objetos.modelo.Rectangulo;
public class RectanguloBaseNegativoCeroComposite extends ValidatorComposite {

	public RectanguloBaseNegativoCeroComposite() {	
	}

	@Override
	public boolean isMe() {
		return figura instanceof Rectangulo;
	}

	@Override
	public boolean validar() {
		Rectangulo rec = (Rectangulo)figura;
		return rec.getBase()<=0;
	}

	@Override
	public String getError() {
		return "LA BASE DEBE SER MAYOR QUE 0";
	}
}