package objetos.controller.composite;
import objetos.modelo.Rectangulo;

public class RectanguloAlturaNegativoCeroComposite extends ValidatorComposite {

	public RectanguloAlturaNegativoCeroComposite() {
	}

	@Override
	public boolean isMe() {
		return figura instanceof Rectangulo;
	}

	@Override
	public boolean validar() {
		Rectangulo rec = (Rectangulo)figura;
		return rec.getAltura()<=0;
	}

	@Override
	public String getError() {
		return "LA ALTURA DEBE SER MAYOR QUE 0";
	}
}