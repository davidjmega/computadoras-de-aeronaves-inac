package objetos.controller;
import objetos.controller.composite.ValidatorComposite;
import objetos.modelo.Figura;
import objetos.modelo.exception.FiguraException;
public class FiguraController implements Controller {

	public FiguraController() {
	}

	@Override
	public void addHandler(Figura fig) throws FiguraException {
		String strErrores=ValidatorComposite.getErrores(fig);
		if(!strErrores.isEmpty())
			throw new FiguraException(strErrores);
	}

	@Override
	public void leerHandler(Figura fig) throws FiguraException {
	}

	@Override
	public void modifyHandler(Figura fig) throws FiguraException {
	}

	@Override
	public void remomveHandler(Figura fig) throws FiguraException {
	}
}
