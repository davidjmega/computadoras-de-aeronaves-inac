package objetos.view;
import java.awt.EventQueue;
import java.awt.Font;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import objetos.controller.FiguraController;
import objetos.modelo.Circulo;
import objetos.modelo.Cuadrado;
import objetos.modelo.Figura;
import objetos.modelo.Rectangulo;
import objetos.modelo.Triangulo;
import objetos.modelo.Poligono;
import objetos.modelo.exception.FiguraException;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.text.DecimalFormat;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;
import java.awt.Color;

public class PantallaFigura {

	private JFrame frame;
	private JTextField textValor;
	private JTable tablFiguras;

	private List<Figura> figuras;
	private String arrayfiguras[][];
	private Figura figuraAmodificarEliminar;
	private JTextField textNombre;
	private JTextField textBase;
	private JTextField txtLado;
	private JTextField textAltura;
	private JTextField textCantidadLados;
	private JTextField textApotema;
	private JLabel lblSuperficieMaxima;

	private FiguraController figuraController = new FiguraController();

	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					PantallaFigura window = new PantallaFigura();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public PantallaFigura() {
		initialize();
	}

	private void initialize() {
		frame = new JFrame();
		frame.setBounds(10, 10, 1024, 800);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.getContentPane().setLayout(null);
		
		JLabel lblNewLabel = new JLabel("FIGURAS GEOMETRICAS");
		lblNewLabel.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 24));
		lblNewLabel.setBounds(345, 27, 262, 29);
		frame.getContentPane().add(lblNewLabel);
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(null, "UNA VARIABLE", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.setBounds(10, 136, 462, 106);
		frame.getContentPane().add(panel);
		panel.setLayout(null);
		
		JLabel lblValor = new JLabel("LADO/RADIO");
		lblValor.setBounds(10, 30, 99, 20);
		panel.add(lblValor);
		lblValor.setFont(new Font("Tahoma", Font.BOLD, 16));
		
		textValor = new JTextField();
		textValor.setBounds(135, 30, 112, 20);
		panel.add(textValor);
		textValor.setFont(new Font("Tahoma", Font.BOLD, 16));
		textValor.setColumns(10);
		
		JButton btnCrearCuadrado = new JButton("CREAR CUADRO");
		btnCrearCuadrado.setBounds(20, 57, 176, 29);
		panel.add(btnCrearCuadrado);
		btnCrearCuadrado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Cuadrado cua =new Cuadrado(textNombre.getText(), Float.parseFloat(textValor.getText()));
				try {
					figuraController.addHandler(cua);
					figuras.add(cua);
					
					llenarGrilla(figuras);
					limpiarCampos();
				} catch (FiguraException e1) {
					JOptionPane.showMessageDialog(null, e1.getMessage());
					e1.printStackTrace();
				}
					}
		});
		btnCrearCuadrado.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 16));
		
		JButton btnCrearCirculo = new JButton("CREAR CIRCULO");
		btnCrearCirculo.setBounds(237, 61, 176, 29);
		panel.add(btnCrearCirculo);
		btnCrearCirculo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Circulo cir = new Circulo(textNombre.getText(), Float.parseFloat(textValor.getText()));
				try {
					figuraController.addHandler(cir);
					figuras.add(cir);
					
					llenarGrilla(figuras);
					limpiarCampos();
				} catch (FiguraException e1) {
					JOptionPane.showInternalMessageDialog(null, e1.getMessage());
					e1.printStackTrace();
				}		
			}
		});
		btnCrearCirculo.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 16));
		
		JButton btnModificar = new JButton("MODIFICAR");
		btnModificar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(figuraAmodificarEliminar instanceof Cuadrado){
					Cuadrado cuad = (Cuadrado) figuraAmodificarEliminar;
					cuad.setNombre(textNombre.getText());
					cuad.setLado(Float.parseFloat(textValor.getText()));
				}
				else if(figuraAmodificarEliminar instanceof Circulo){
					Circulo cir = (Circulo) figuraAmodificarEliminar;
					cir.setNombre(textNombre.getText());
					cir.setRadio(Float.parseFloat(textValor.getText()));
				}
				else if(figuraAmodificarEliminar instanceof Triangulo){
					Triangulo tri = (Triangulo) figuraAmodificarEliminar;
					tri.setNombre(textNombre.getText());
					tri.setAltura(textAltura.getText());
					tri.setBase(textBase.getText());
				}
				else if(figuraAmodificarEliminar instanceof Rectangulo){
					Rectangulo rec = (Rectangulo) figuraAmodificarEliminar;
					rec.setNombre(textNombre.getText());
					rec.setBase(Float.parseFloat(textBase.getText()));
					rec.setAltura(Float.parseFloat(textAltura.getText()));
				}
				else if (figuraAmodificarEliminar instanceof Poligono) {
					Poligono pol = (Poligono) figuraAmodificarEliminar;
					pol.setNombre(textNombre.getText());
					pol.setApotema(Float.parseFloat(textApotema.getText()));
					pol.setLado(Integer.parseInt(txtLado.getText()));
					pol.setCantidadDeLados(Integer.parseInt(textCantidadLados.getText()));
				}
							
				llenarGrilla(figuras);
				figuraAmodificarEliminar = null;
				limpiarCampos();
			}
		});
		btnModificar.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 16));
		btnModificar.setBounds(221, 621, 176, 30);
		frame.getContentPane().add(btnModificar);
		
		JButton btnEliminar = new JButton("ELIMINAR");
		btnEliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				figuras.remove(figuraAmodificarEliminar);
				llenarGrilla(figuras);
				limpiarCampos();
				figuraAmodificarEliminar=null;
			}
		});
		btnEliminar.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 16));
		btnEliminar.setBounds(436, 621, 176, 31);
		frame.getContentPane().add(btnEliminar);
		
		JButton btnLimpiarCampos = new JButton("LIMPIAR CAMPOS");
		btnLimpiarCampos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				limpiarCampos();
			}
		});
		btnLimpiarCampos.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 16));
		btnLimpiarCampos.setBounds(637, 620, 176, 32);
		frame.getContentPane().add(btnLimpiarCampos);
		
		JLabel lblNombre = new JLabel("NOMBRE");
		lblNombre.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblNombre.setBounds(193, 78, 99, 20);
		frame.getContentPane().add(lblNombre);
		
		textNombre = new JTextField();
		textNombre.setFont(new Font("Tahoma", Font.BOLD, 16));
		textNombre.setColumns(10);
		textNombre.setBounds(318, 78, 112, 20);
		frame.getContentPane().add(textNombre);
		
		JPanel panel_1 = new JPanel();
		panel_1.setLayout(null);
		panel_1.setBorder(new TitledBorder(null, "DOS VARIABLES", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.setBounds(521, 136, 462, 106);
		frame.getContentPane().add(panel_1);
		
		JLabel lblBase = new JLabel("BASE");
		lblBase.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblBase.setBounds(10, 30, 60, 20);
		panel_1.add(lblBase);
		
		textBase = new JTextField();
		textBase.setFont(new Font("Tahoma", Font.BOLD, 16));
		textBase.setColumns(10);
		textBase.setBounds(80, 30, 112, 20);
		panel_1.add(textBase);
		
		JButton btnCrearRectangulo = new JButton("CREAR RECTANGULO");
		btnCrearRectangulo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Rectangulo rec = new Rectangulo(textNombre.getText(), Float.parseFloat(textBase.getText()),Float.parseFloat(textAltura.getText()));
				try {
					figuraController.addHandler(rec);
					figuras.add(rec);
					
					llenarGrilla(figuras);
					limpiarCampos();
				} catch (FiguraException e1) {
					JOptionPane.showMessageDialog(null, e1.getMessage());
					e1.printStackTrace();
				}		
			}
		});
		btnCrearRectangulo.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 16));
		btnCrearRectangulo.setBounds(20, 57, 176, 29);
		panel_1.add(btnCrearRectangulo);
		
		JButton btnCrearTriangulo = new JButton("CREAR TRIANGULO");
		btnCrearTriangulo.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Triangulo tri = new Triangulo(textNombre.getText(),
							Float.parseFloat(textBase.getText()), 
							Float.parseFloat(textAltura.getText()));
				try {
					figuraController.addHandler(tri);
					figuras.add(tri);
					
					llenarGrilla(figuras);
					limpiarCampos();
				} catch (FiguraException e1) {
					JOptionPane.showMessageDialog(null, e1.getMessage());
					e1.printStackTrace();
				}
			}
		});
		btnCrearTriangulo.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 16));
		btnCrearTriangulo.setBounds(237, 61, 176, 29);
		panel_1.add(btnCrearTriangulo);
		
		JLabel lblAltura = new JLabel("ALTURA");
		lblAltura.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblAltura.setBounds(237, 30, 60, 20);
		panel_1.add(lblAltura);
		
		textAltura = new JTextField();
		textAltura.setFont(new Font("Tahoma", Font.BOLD, 16));
		textAltura.setColumns(10);
		textAltura.setBounds(307, 30, 112, 20);
		panel_1.add(textAltura);
		
		JPanel panel_2 = new JPanel();
		panel_2.setLayout(null);
		panel_2.setBorder(new TitledBorder(null, "TRES VARIABLES", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_2.setBounds(270, 253, 573, 158);
		frame.getContentPane().add(panel_2);
		
		JLabel lblValor_2 = new JLabel("LADO");
		lblValor_2.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblValor_2.setBounds(10, 30, 54, 20);
		panel_2.add(lblValor_2);
		
		txtLado = new JTextField();
		txtLado.setFont(new Font("Tahoma", Font.BOLD, 16));
		txtLado.setColumns(10);
		txtLado.setBounds(74, 30, 112, 20);
		panel_2.add(txtLado);
		
		JButton btnCrearPoligono = new JButton("CREAR POLIGONO");
		btnCrearPoligono.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Poligono pol = new Poligono(textNombre.getText(), 
						Float.parseFloat(textApotema.getText()), 
						Float.parseFloat(txtLado.getText()), 
						Float.parseFloat(textCantidadLados.getText()));
				try {
					figuraController.addHandler(pol);
					figuras.add(pol);
					
					llenarGrilla(figuras);
					limpiarCampos();
				} catch (FiguraException e1) {
					JOptionPane.showMessageDialog(null, e1.getMessage());
					e1.printStackTrace();
				}
			}
		});
		btnCrearPoligono.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 16));
		btnCrearPoligono.setBounds(171, 106, 176, 29);
		panel_2.add(btnCrearPoligono);
		
		JLabel lblValor_2_1 = new JLabel("CANTIDAD DE LADOS");
		lblValor_2_1.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblValor_2_1.setBounds(227, 30, 176, 20);
		panel_2.add(lblValor_2_1);
		
		textCantidadLados = new JTextField();
		textCantidadLados.setFont(new Font("Tahoma", Font.BOLD, 16));
		textCantidadLados.setColumns(10);
		textCantidadLados.setBounds(419, 30, 112, 20);
		panel_2.add(textCantidadLados);
		
		JLabel lblValor_2_2 = new JLabel("APOTEMA");
		lblValor_2_2.setFont(new Font("Tahoma", Font.BOLD, 16));
		lblValor_2_2.setBounds(112, 61, 74, 20);
		panel_2.add(lblValor_2_2);
		
		textApotema = new JTextField();
		textApotema.setFont(new Font("Tahoma", Font.BOLD, 16));
		textApotema.setColumns(10);
		textApotema.setBounds(200, 61, 112, 20);
		panel_2.add(textApotema);
				
				JScrollPane scrollPane = new JScrollPane();
				scrollPane.setBounds(270, 419, 573, 173);
				frame.getContentPane().add(scrollPane);
				
				tablFiguras = new JTable();
				tablFiguras.addMouseListener(new MouseAdapter() {
					@Override
					public void mouseClicked(MouseEvent e) {
						
						figuraAmodificarEliminar= figuras.get(tablFiguras.getSelectedRow());
						
						if(figuraAmodificarEliminar instanceof Cuadrado)
							asignarValores((Cuadrado)figuraAmodificarEliminar);
						else if(figuraAmodificarEliminar instanceof Circulo)
							asignarValores((Circulo)figuraAmodificarEliminar);
						else if(figuraAmodificarEliminar instanceof Rectangulo)
							asignarValores((Rectangulo)figuraAmodificarEliminar);
						else if(figuraAmodificarEliminar instanceof Triangulo)	
							asignarValores((Triangulo)figuraAmodificarEliminar);
						else if(figuraAmodificarEliminar instanceof Poligono )
							asignarValores((Poligono)figuraAmodificarEliminar);
					}
				});
				tablFiguras.setFont(new Font("Tahoma", Font.BOLD, 16));
				
						scrollPane.setViewportView(tablFiguras);
						
						JLabel lblNewLabel_1 = new JLabel("MAXIMA SUPERFICIE");
						lblNewLabel_1.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 24));
						lblNewLabel_1.setBounds(475, 67, 242, 29);
						frame.getContentPane().add(lblNewLabel_1);
						
						lblSuperficieMaxima = new JLabel("");
						lblSuperficieMaxima.setBackground(Color.ORANGE);
						lblSuperficieMaxima.setOpaque(true);
						lblSuperficieMaxima.setFont(new Font("Tahoma", Font.BOLD, 20));
						lblSuperficieMaxima.setBounds(761, 71, 141, 25);
						frame.getContentPane().add(lblSuperficieMaxima);
		asignarValoresIniciales();
	}
	private void asignarValores(Cuadrado pCua){
		textNombre.setText(pCua.getNombre());
		textValor.setText(Float.toString(pCua.getLado()));
		
	}
	private void asignarValores(Circulo pCir){
		textNombre.setText(pCir.getNombre());
		textValor.setText(Float.toString(pCir.getRadio()));
	}
	private void asignarValores(Rectangulo pRec){
		textNombre.setText(pRec.getNombre());
		textBase.setText(Float.toString(pRec.getBase()));
		textAltura.setText(Float.toString(pRec.getAltura()));
	}
	private void asignarValores(Triangulo pTri){
		textNombre.setText(pTri.getNombre());
		textBase.setText(Float.toString(pTri.getBase()));
		textAltura.setText(Float.toString(pTri.getAltura()));
	}
	private void asignarValores(Poligono pPol) {
		textNombre.setText(pPol.getNombre());
		textApotema.setText(Float.toString(pPol.getApotema()));
		textCantidadLados.setText(Float.toString(pPol.getCantidadDeLados()));
		txtLado.setText(Float.toString(pPol.getLado()));
	}
	
	private void llenarGrilla(List<Figura> pFiguras){
		int fila =0 ;
		arrayfiguras = new String[pFiguras.size()][4];

		DecimalFormat df = new DecimalFormat("#.##");
		for (Figura figura : pFiguras) {
			for(int col=0;col<4;col++){
				switch (col) {
				case 0:
					arrayfiguras[fila][col] = figura.getNombre();					
					break;
				case 1:
					arrayfiguras[fila][col] = figura.getValores();					
					break;					
				case 2:
					arrayfiguras[fila][col] = df.format(figura.calcularPerimetro());					
					break;
				case 3:
					arrayfiguras[fila][col] = df.format(figura.calcularSuperficie());					
					break;

				default:
					break;
				}				
			}
			fila++;
		}
		lblSuperficieMaxima.setText(df.format(Figura.getMaximaSuperficie()));

		tablFiguras.setModel(new DefaultTableModel(
				arrayfiguras,
				new String[] {
					"Nombre","valores", "perimetro", "superficie"
				}
			));
	}
	private void asignarValoresIniciales(){
		figuras= new ArrayList<Figura>();
		figuras.add(new Cuadrado("cuadrado 1", 15));
		figuras.add(new Circulo("circulo 1", 10));
		figuras.add(new Cuadrado("cuadrado 2", 20));
		figuras.add(new Circulo("circulo 2", 30));
		
		llenarGrilla(figuras);		
	}
	private void limpiarCampos() {
		textNombre.setText("");
		textValor.setText("");
		textBase.setText("");
		textAltura.setText("");	
	}
}
